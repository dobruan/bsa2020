﻿using CoolParking.BL.Models;
using CoolParkingApp.Menu;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace CoolParkingApp
{
    class Program
    {
        static void Main(string[] args)
        {
            
            Console.OutputEncoding = System.Text.Encoding.Unicode;
            MenuItem.PopulateMenuItems();
            MenuItem.MainMenu();

            /*Parking parking = Parking.getInstance();
           
            Vehicle myCar = new Vehicle("AS-4545-QW", VehicleType.PassengerCar, 100);
            Vehicle myBus = new Vehicle("CA-8594-WD", VehicleType.Bus, 200);


            

                
            parking.vehicleOnParking.Add(myBus);
            parking.vehicleOnParking.Add(myCar);

            parking.vehicleOnParking.Remove(parking.vehicleOnParking.Find(x => x.Id == "AS-4545-QW"));

            myCar.Balance += 100;
            Console.WriteLine(myCar.Balance);
            */
            
            Console.WriteLine();
        }
    }
}