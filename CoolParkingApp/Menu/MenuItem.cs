﻿using CoolParking.BL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace CoolParkingApp.Menu
{
    public class MenuItem
    {
        public string Description { get; set; }
        public Action Execute { get; set; }


        
        private static List<MenuItem> MenuItems;

       
        /*
         Вывести на экран текущий баланс Парковки.
·         Вывести на экран сумму заработанных денег за текущий период (до записи в лог).
·         Вывести на экран количество свободных/занятых мест на парковке.
·         Вывести на экран все Транзакции Парковки за текущий период (до записи в лог).
·         Вывести историю транзакций (считав данные из файла Transactions.log).
·         Вывести на экран список Тр. средств находящихся на Паркинге.
·         Поставить Тр. средство на Паркинг.
·         Забрать транспортное средство с Паркинга.
·         Пополнить баланс конкретного Тр. средства.

             */
        public static void PopulateMenuItems()
        {
            MenuItems = new List<MenuItem>
            {
        new MenuItem {Description = "Поточний баланс парковки", Execute = ViewBalance},
        new MenuItem {Description = "Створити транспортний засіб", Execute = AddVehicle},


        new MenuItem {Description = "Заробіток", Execute = Deposit},
        new MenuItem {Description = "Кількість вільних/зайнятих міст", Execute = Withdraw},

        new MenuItem {Description = "Поточні транзакції", Execute = MyItem},

        new MenuItem {Description = "Вся історія транзакцій", Execute = MyItem},

        new MenuItem {Description = "Всі транспортні засоби на парковці", Execute = MyItem},

        new MenuItem {Description = "Поставити транспортний засіб на парковку", Execute = MyItem},
        new MenuItem {Description = "Забрати транспортний засіб з парковки", Execute = MyItem},
        
        new MenuItem {Description = "Поповнити баланс транспортного засобу", Execute = MyItem},
        
        


        new MenuItem {Description = "Exit", Execute = Exit}
            };
        }

        private static void Exit()
        {
            System.Environment.Exit(0);
        }

        private static void AddVehicle() 
        {
            
            
            VehicleType vType= VehicleType.Motorcycle;
            int type=0;
            while (((type>5)||(type<= 0))) {
                Console.WriteLine("Виберить тип транспортного засобу: 1. Легкове авто 2. Вантажівка 3.Автобус 4.Мотоцикл");
                try
                {
                    type = Convert.ToInt32(Console.ReadLine());
                }
                catch (Exception)
                {
                    Console.WriteLine("Зробіть правильний вибір.");
                    continue;
                }

                switch (type)
                {
                    case 1:
                        vType = VehicleType.PassengerCar;
                        break;
                    case 2:
                        vType = VehicleType.Truck;
                        break;
                    case 3:
                        vType = VehicleType.Bus;
                        break;
                    case 4:
                        vType = VehicleType.Motorcycle;
                        break;
                }
            }
           
             Console.WriteLine("Вкажіть сумму для балансу транспортного засобу:");

            decimal sum = Convert.ToDecimal(Console.ReadLine());
            Vehicle vehicle = new Vehicle( vType, sum);
            Parking parking = Parking.getInstance();
            parking.vehicleOnParking.Add(vehicle);
            GoToMainMenu();
        }

        private static void MyItem()
        {
            string myComand = Console.ReadLine();
            ClearAndShowHeading(myComand);
            GoToMainMenu();
        }

        private static void ViewBalance()
        {
            ClearAndShowHeading("Account Balance");
            GoToMainMenu();
        }

        private static void Deposit()
        {
            ClearAndShowHeading("Account Deposit");
            GoToMainMenu();
        }

        private static void Withdraw()
        {
            ClearAndShowHeading("Account Withdrawl");
            GoToMainMenu();
        }

        private static void ClearAndShowHeading(string heading)
        {
            Console.Clear();
            Console.WriteLine(heading);
            Console.WriteLine(new string('-', heading?.Length ?? 0));
        }

        private static void GoToMainMenu()
        {
            Console.Write("\nPress any key to go to the main menu...");
            Console.ReadKey();
            MainMenu();
        }

        public static void MainMenu()
        {
            
            ClearAndShowHeading("Головне меню");

            // Write out the menu options
            for (int i = 0; i < MenuItems.Count; i++)
            {
                Console.WriteLine($"  {i + 1}. {MenuItems[i].Description}");
            }

            // Get the cursor position for later use 
            // (to clear the line if they enter invalid input)
            int cursorTop = Console.CursorTop + 1;
            int userInput;

            // Get the user input
            do
            {
                // These three lines clear the previous input so we can re-display the prompt
                Console.SetCursorPosition(0, cursorTop);
                Console.Write(new string(' ', Console.WindowWidth));
                Console.SetCursorPosition(0, cursorTop);

                Console.Write($"Enter a choice (1 - {MenuItems.Count}): ");
            } while (!int.TryParse(Console.ReadLine(), out userInput) ||
                     userInput < 1 || userInput > MenuItems.Count);

            // Execute the menu item function
            MenuItems[userInput - 1].Execute();
        }
    }
}
